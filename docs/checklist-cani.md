
# Can I share my data - Checklist
Use this check list to guide your decision on whether you are ready to share your data and where to share it. Return to the [decision questions](./decision-tree-refactored.md) for more detail on each question.


## 1. Is sharing restricted under Intellectual Properties rights?
- [ ] 1.1. Has your funder or industry partner approved data sharing?
- [ ] 1.2. Have you investigated commercial potential of your data?

## 2. Are you sharing data acquired from living humans?
- [ ] 2.1. Human alive at the time of data collection.
- [ ] 2.2. Ex vivo human.
- [ ] 2.3. Non-human animal.

## 3. Have you considered data governance with respect to sharing?
- [ ] 3.1. Have you discussed open data sharing in your data management plan?
- [ ] 3.2. Have you completed a Data Protection Impact Assessment (DPIA) screening form which references data sharing?

## 4. Do you have ethical approvals in place?
- [ ] 4.1. Have you described data sharing in your ethics application?
- [ ] 4.2. Has your participant consented to data sharing?

## 5. Have you de-identified your data?
- [ ] 5.1. Have you removed any "direct identifiers" in your data?
- [ ] 5.2. Are your imaging data in participant space?
- [ ] 5.3. Have your Participant IDs been protected?
- [ ] 5.4. Have "indirect identifiers" such as age, gender, handedness or disease status been protected?
- [ ] 5.5. Have unique dicom fields been scrubbed?
- [ ] 5.6. Have unique fields in .json sidecar files been scrubbed?
- [ ] 5.7. Have images been defaced?
- [ ] 5.8. Have you conducted and prepared to share a quality control analysis?

## 6. Have you prepared the data according to community standards?
- [ ] 6.1. For example BIDS for MRI.

## 7. Have you prepared the metadata to make your data FAIR?
- [ ] 7.1. Are behavioural and clinical covariates appropriately described?
- [ ] 7.2. Are you able to share the image acquisition protocol?
- [ ] 7.3. Has the experimental protocol been described and made ready to share with the data?

## 8. Can access to your data be restricted?
- [ ] 8.1. Can you create a "reviewer only" link?
- [ ] 8.2. Can you restrict access to bonafide researchers only?

## 9. Can you ensure that you are appropriately acknowledged when your data are reused?
- [ ] 9.1. Can you create a doi for your data?
- [ ] 9.2. Can you select a license which requires attribution?

## 10. Would you like to impose custom terms around the reuse of your data?
- [ ]  10.1. Would you like to impose requirements for authorship?
- [ ]  10.2. Would you like to impose restrictions on resharing?
- [ ]  10.3. Would you like to explicitly prohibit attempts to reidentify participants in your data?
- [ ]  10.4. Would you like to add any funder requirements in the reuse of your data?
