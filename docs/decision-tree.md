# Open data decision tree

## Overview
We have divided the data sharing process into five stages. Each stage should be reviewed in turn to see what path your project should follow.
![](../img/process-overview-key.png)

### Start and Stop
In each process there is a "start" point, and in some cases a hard "stop". A "stop" is reached where the project has not achieved the mandatory stages as required by University policy or GDPR.

### Navigating through the processes
Each process links to the next in the "Go to next process" box. Not all data types will follow all five steps sequentially; some project may skips as indicated by following the tree according to the answers given.

### Recommended and mandatory steps
Each process has both "recommended" and "mandatory" steps. Mandatory steps fulfil your requirements according to University and GDPR. Following the recommended steps brings your project into alignment with the growing consensus for data sharing according to the [FAIR
principles](https://www.go-fair.org/fair-principles/)

<a name="ethics-rdm"></a>
## Process 1 - Data management, data security and ethics
Good data management is essentail for effective data sharing, for audit purposes and to assess the security and privacy risks of your data. This process ensures that you have completed all governance reviews as required by central Information Compliance, and that you have the correct ethical approvals in place to share your data.
![](../img/p1.png)

<a name="protected"></a>
## Process 2 - Protected features of the data
Different modalities, participants and data sources carry differential risks. This process will assess how your data should be handelled based on what you intend to share.
![](../img/p2.png)

<a name="deidentify"></a>
## Process 3 - Deidentification
Most of the data we handle at WIN cannot be fully anonymised as each participant contributes unique information. Instead we aim to maximally deidentify the data while maintining sufficient granularity for analysis. This process highlights which parts of your data may contain identifiable features and how they should be handelled. It also suggests community standards in data structure and quality control.

> See also:
> - [Appendix 1 - DICOM headers to be scrubbed](./decision-tree-appendicies.md#appendix-1-dicom-headers-to-be-scrubbed)
> - [Appendix 2 - BIDS compliance](./decision-tree-appendicies.md#appendix-2-bids-compliance)
> - [Appendix 3 - Scrubbing .json files](./decision-tree-appendicies.md#appendix-3-scrubbing-json-files)
> - [Appendix 4 - Quality control](./decision-tree-appendicies.md#appendix-4-quality-control)
> - [Appendix 5 - Defacing](./decision-tree-appendicies.md#appendix-5-defacing)

![](../img/p3.png)

<a name="metadata"></a>
## Process 4 - Metadata
High quality metadata enable others to reuse your data effectively. This process suggests where metadata may be valuable and how they can be compiled.

> See also:
> - [Appendix 2 - BIDS compliance](./decision-tree-appendicies.md#appendix-2-bids-compliance)

![](../img/p4.png)

<a name="sharing-attribution"></a>
## Process 5 - Sharing and attribution
Your shared research data are a valuable output of your work. This process describes adding your data to the XNAT repository and generating a citable digital object identifier (DOI). It also guides you through the selection of an appropriate data usage agreement (DUA) which subsequent users of your data will agree to follow.

> See also:
> - [Appendix 6 - Upload to XNAT](./decision-tree-appendicies.md#appendix-6-upload-to-xnat)
> - [Appendix 7 - Data freeze](./decision-tree-appendicies.md#appendix-7-data-freeze)
> - [Appendix 8 - Digital object identifiers (DOI)](./decision-tree-appendicies.md#appendix-8-digital-object-identifiers-doi)
> - [Appendix 9 - Data usage agreement (DUA)](./decision-tree-appendicies.md#appendix-9-data-usage-agreement-dua)

![](../img/p5.png)

# References
[DPIA screening and DPIA/DPA templates]
(https://compliance.admin.ox.ac.uk/data-protection-forms#collapse1091641)

Jones and Ford, 2018. "Privacy, confidentiality and practicalities in data linkage. National Statistical Quality Review". [https://cronfa.swan.ac.uk/Record/cronfa53688](https://cronfa.swan.ac.uk/Record/cronfa53688)

Harron et al, 2017. "Challenges in administrative data linkage for research". Big Data & Society. December 2017. [doi:10.1177/2053951717745678](https://journals.sagepub.com/doi/10.1177/2053951717745678)

[Open Brain Consent GDPR Edition](https://open-brain-consent.readthedocs.io/en/stable/gdpr/index.html)

[CUREC Best Practice Guidance (BPG) 09 "Data collection, protection and management".](https://researchsupport.admin.ox.ac.uk/files/bpg09datacollectionandmanagementpdf)
