# Open data sharing participant information sheet

Dear Participant,

As well as participating in the current research project, you are being invited to add your data to an "open data repository" for other researchers outside of the University of Oxford to access.

Before you decide whether to contribute to this initiative, it is important for you to understand what the open data repository is, why it has been developed, and how your data will be managed to ensure your privacy and protection.

Please take time to read the following information carefully. We would like to emphasise that giving your consent for your data to be added to the open data repository is optional and is not a requirement to participate in the current research project you have been recruited to.

This information sheet contains UK GDPR-specific wording that relates to privacy implications in the intention to process and share your participant data. As such, this information should be considered to be the [GDPR Privacy Notice](https://www.england.nhs.uk/nhse-nhsi-privacy-notice/what-is-a-privacy-notice/).

> A privacy notice should identify who the data controller is, with contact details for its Data Protection Officer. It should also explain the purposes for which personal data are collected and used, how the data are used and disclosed, how long it is kept, and the controller’s legal basis for processing.

## What is an "Open Data Repository"?

> we need illustrations and videos. Maybe one of those "speed illustration" videos (e.g. [videoscribe](https://www.videoscribe.co/en)

An open data repository is an online database from which professional scientists can share the data they have collected in a research study. The data are typically shared with scientists at other research institutions around the world.

Open data repositories have been developed to make it easier to share data efficiently and effectively with other researchers. Easy access to collected data has multiple benefits, including:

1. Making it easier for others to verify the findings of a study;
2. Making it easier to conduct further research on the same data, which reduces the overall costs of research as new data are not always needed. Data collection is an expensive process which is typically funded by Government taxes or Charities;
3. Reducing the amount of time it takes to translate a research study finding into a scalable health treatment through a combination of the above.

While these benefits are highly desirable, we recognise that there are explicit risks to sharing your data. We take very seriously the responsibility to ensure that your data are shared only with individuals who intend to use it for genuine research purposes and agree to protect your privacy.

In the following sections we describe how we will manage access to your data, and how we will ensure your privacy and protection throughout.

...

## How will the WIN open data repository work?
> WIN Researchers add data with consent. Other researchers are invited to access and use in a way that benefits their research interests. Always under the agreement that it is non-commercial.

> What data will be shared (brain, behavioural)

> Jess com: I think we should be very clear here that they don't have to undergo any further research procedures to take part in the repository, we will just be using data that is already being collected as part of the research study they have already signed up to.

## Why have I been invited?

You have been invited because you are already taking part in another research study involving taking pictures of your brain. Our database is trying to include as many pictures of peoples brains as possible so that lots of data is available for research.

> Jess com: I've taken some additional headings from the template PIS protocol that is used for studies sponsored by CTRG.

## Do I have to sign up to the WIN open data repository?
No. Signing up to the repository is completely voluntary. Even if you do sign up, you can withdraw at a later date without giving a reason (further details on this can be found on the 'What if I change my mind?' section below). If you choose not to sign up to the WIN open data repository then you can still take part in the research study that you have already signed up for.

## What will happen to my data if I sign up?

All the data that is collected for the purposes of the study you have been recruited for will be considered for submission to the open data repository. For some data types we will undertake specific measures to ensure it is not possible to identify you from the data, while retaining the integrity and value of the data for further research.

> all this needs to be re-written for lay language...

### Will my taking part in the repository be kept confidential?

To the best of our knowledge, the data we submit to the repository will not contain information that can directly identify you using reasonable means. The data will be given a code, so people will not know your name or which data are yours. Your name and other directly identifiable information will be omitted. Identifiable facial features on the brain scans will also be removed.

> do we need pictures of defacing? Will that just be confusing to people? Feel like we need to do some PPI on this...
> Jess com: no i'm not sure it's worth going into defacing... I think it just overcomplicates it and I think most people would assume that you can't see their face from a brain scan anyway. Could still do some PPI though.

Data can only be linked back to you using information available only to the data processor (the people involved in the study you have been recruited to). This linkage information will remain safely stored in the local research institute and never released on the open data repository. The data cannot be linked back to you in reports or publications about the study. However, by using additional data linked to your name (for example brain scans obtained from your medical records) one could potentially associate your imaging or other information in our database back to you. The risks of accessing such data from our servers has been assessed is considered to be low (see the university/centre Data Privacy Impact Assessment @<URL>).
> need to add and DPIA assessment

For the reasons described above, your data can never be considered fully anonymous. We will, however, make our best efforts to ensure all identifiable information has been removed (your data will be "de-identified") while retaining the important and valuable information which you have volunteered for the purposes of research.


### Who will have access to my data?

Your de-identified data will be advertised as available for re-use via our open data repository. Other researchers will be invited to access your de-identified data when the study you have been recruited for is presented at research conferences and journal publications. Other researchers will also be able to search our open data repository to look for specific data types, for example data in certain formats or relating to a specific topic.

Once another researcher has requested to access your data for reuse, they will be required to make a free account on our system so we can verify their interests as a genuine researcher. This verification will be conducted through the existence of an ORCID ID, a free persistent identifier which asserts a researchers credentials by enabling them to connect all of their research works and professional affiliations which are visible to everyone (for more information see ["Does an ORCID ID assure my identity"](https://support.orcid.org/hc/en-us/articles/360006972413-Does-an-ORCID-iD-assure-my-identity-)).

Once the genuine interests of a researcher have been verified, they will be asked to digitally sign a [data usage agreement](data-usage-agreement.md) described the terms of the access and use of your de-identified data. Importantly, they will agree to make no attempts to re-identify you or share your data any further. Anyone wishing to use your data will always have to go through our system and agree to the same terms of usage.

### How long will my data be kept for?
There is no plan to delete your data as they can be re-used for legitimate research interest. We will however re-evaluate every <number of years> years if it is worthwhile keeping them.

> Jess com: If we go for a research database through CTRG then you can get approval for 5 years. Every 5 years you have to request an extension for another 5 years and review where you're at etc. This is what they've done for the BHC.

## What if I change my mind?
You can withdraw your consent to the use of your personal data at any time without any effect on your clinical care or your involvement in other research studies. This applies to this study and also to the sharing for future research. You have however to understand that once shared with other institutions, it is impossible to remove your data from such copies.

## Passing your data on to countries outside of the UK and European Union
Your encoded data can also be accessed by and sent to countries outside the EU. This is necessary so that non-EU based scientists can run analyses to verify the scientific results produced from this study or for future unrelated research in the field of medical and cognitive neuroscience. In those countries, the EU rules on the protection of your personal data do not apply. However, your privacy will be protected at an equal level, by means of a [Data User Agreement](./data-usage-agreement.md).

## More information about your rights regarding processing of your database
For general information about your rights regarding the processing of your personal data, you can consult the website of the <Institution> Data Protection Authority.

If you have questions about your rights, please contact the person responsible for the processing of your personal data. For this study, that is:

<contact information> (see Appendix for contact details)

If you have questions or complaints about the processing of your personal data, we advise you to first contact the research location. You can also contact the Data Protection Officer of <Institution> (see the contact details in Appendix ) or the <Institution> Data Protection Authority.
