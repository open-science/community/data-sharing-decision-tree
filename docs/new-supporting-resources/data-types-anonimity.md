# Data sources and GDPR categorisation

The below refers only to data obtained from human participants.

| Data source | Can it be anonymised?  | references |
|---|---|---|
| Pupilometry  | no | "various kinds of sensitive inferences can be drawn from eye tracking data. Among other categories of personal data, recorded visual behavior can implicitly contain information about a person’s biometric identity, personality traits, ethnic background, age, gender, emotions, fears, preferences, skills and abilities, drug habits, levels of sleepiness and intoxication, and physical and mental health condition. To some extent, even distinct stages of cognitive information processing are discernable from gaze data. Thus, devices with eye tracking capability have the potential to implicitly capture much more information than a user wishes and expects to reveal. Some of the categories of personal information listed above constitute special category data, for which particular protection is prescribed by the EU’s General Data Protection Regulation (Art. 9 GDPR)."" ([Kröger, Lutz and Müller, 2020](https://link.springer.com/chapter/10.1007/978-3-030-42504-3_15))  |
| whole brain MRI  | no |   |
| incomplete field of view MRI | yes  |   |
| aggregated participant MRI data  | yes, if &gt 5 participants aggregated  |   |
| models derived from personal data | yes | "The Court of Justice of the European Union (CJEU) has ruled that the data in the legal analysis contained in that document, are personal data, whereas, by contrast, that analysis cannot in itself be so classified [16]." ([Klavan, Tavast & Kelli, 2018](https://ebooks.iospress.nl/publication/50306)) |
| k-space values  |   |   |
| participant identifiers (name, address)  | no  |   |
| demographics (gender, handeness, age)  | yes, if &gt 5 participants per cell  |   |
| Psychological or psychiatric questionnaires  |   |   |
| behavioural data (reaction times and accuracy) |   |   |
| respiratory measurement  |   |   |
| cardiac or pulse oximetry  |   |   |
| EEG  |   |   |
| MEG  |   |   |
| Electromyography  |   |   |
| subcortical electrode recordings  |   |   |
| genetic data  | no  | "In most cases, you process genetic information to learn something about a specific identified individual and to inform you about taking some action in relation to them. This is clearly personal data – and special category genetic data - for the purposes of the UK GDPR."[...] "n practice, genetic analysis which includes enough genetic markers to be unique to an individual is personal data and special category genetic data, even if you have removed other names or identifiers. And any genetic test results which are linked to a specific biological sample are usually personal data, even if the results themselves are not unique to the individual, because the sample is by its nature specific to an individual and provides the link back to their specific genetic identity." ([ICO](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/special-category-data/what-is-special-category-data/#scd3))  |
| cell counts (e.g. inflammatory)  |   |   |
| hormone measures (e.g. cortisol)   |   |   |
| actigraphy  |   |   |
| Skin conductance (galvanic skin response)  |   |   |

## Definitions

### Personal data
[Definition](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/key-definitions/what-is-personal-data/): Information relating to natural persons who can be identified or who are identifiable, directly from the information in question; or who can be indirectly identified from that information in combination with other information.

### Identifiers
[Definition](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/key-definitions/what-is-personal-data/): An individual is ‘identified’ or ‘identifiable’ if you can distinguish them from other individuals. A name is perhaps the most common means of identifying someone. However whether any potential identifier actually identifies an individual depends on the context. A combination of identifiers may be needed to identify an individual.
The UK GDPR provides a non-exhaustive list of identifiers, including: name; location data; an online identifier.
‘Online identifiers’ includes IP addresses and cookie identifiers which may be personal data. Other factors can identify an individual. You don’t have to know someone’s name for them to be directly identifiable, a combination of other identifiers may be sufficient to identify the individual. If an individual is directly identifiable from the information, this may constitute personal data.

*In some circumstances there may be a slight hypothetical possibility that someone might be able to reconstruct the data in such a way that identifies the individual. However, this is not necessarily sufficient to make the individual identifiable in terms of UK GDPR. You must consider all the factors at stake. When considering whether individuals can be identified, you may have to assess the means that could be used by an interested and sufficiently determined person. You have a continuing obligation to consider whether the likelihood of identification has changed over time (for example as a result of technological developments).*

### Biometric data
[Definition](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/special-category-data/what-is-special-category-data/#scd4): ‘Biometric data’ means personal data resulting from specific technical processing relating to the physical, physiological or behavioural characteristics of a natural person, which allow or confirm the unique identification of that natural person, such as facial images or dactyloscopic [fingerprint] data.

### Health data
[Definition](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/special-category-data/what-is-special-category-data/#scd5): ‘Data concerning health’ means personal data related to the physical or mental health of a natural person, including the provision of health care services, which reveal information about his or her health status. Health data can be about an individual’s past, current or future health status. It not only covers specific details of medical conditions, tests or treatment, but includes any related data which reveals anything about the state of someone’s health. Health data can therefore include a wide range of personal data, for example: any information on injury, disease, disability or disease risk, including medical history, medical opinions, diagnosis and clinical treatment; medical examination data, test results, data from medical devices, or data from fitness trackers.

### Data controller
[Definition](https://www.sheffield.ac.uk/polopoly_fs/1.724754!/file/SGREP-Anonymity-Confidentiality-DataProtection.pdf): ‘Data Controller’: the individual or organisation which determines the purposes and means of processing personal data). For research undertaken by staff or students of the University of Sheffield, the Data Controller will usually be the University of Sheffield (i.e. not a particular individual or research team)."

### Anonymisation
Requirements
