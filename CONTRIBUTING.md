# Contributing

How to contribute to this project

---

We want to ensure that every user and contributor feels welcome and included in this project being conducted by the Open WIN Community. We ask all contributors and community members to follow the [Open WIN Participation Guidelines](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/CODE_OF_CONDUCT/) in all community interactions.

**We hope that this guideline document will make it as easy as possible for you to get involved.**

Please follow these guidelines to make sure your contributions can be easily integrated in the projects. As you start contributing to Open WIN projects, don't forget that your ideas are more important than perfect [merge requests (known as "pull requests" in GitHub)](https://opensource.stackexchange.com/questions/352/what-exactly-is-a-pull-request).

## 1. Contact us
In the first instance, we encourage you [connect with the Open WIN Community](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/contact/). Let us know that you are reading the repository and you would like to contribute! You might like to introduce yourself on the #welcome channel on our [Slack workspace](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/contact/#open-win-slack-)! 👋

## 2. Check what we're working on
You can see the things we are working on for this project in the [issue list](https://git.fmrib.ox.ac.uk/open-science/community/data-sharing-decision-tree/-/issues) and [milestones](https://git.fmrib.ox.ac.uk/open-science/community/data-sharing-decision-tree/-/milestones). You are also welcome to suggest any of your own improvements.

## 3. Join us in developing this resource!
### Provide feedback
This project is open for feedback and consultation with WIN researchers. We are seeking this feedback to identify bottlenecks, missing guidance and further refinements.

Consultation has started by inviting a small number of researchers (with a range of data types) to [comment the proposed guidance](.docs/decision-tree.md). This is being achieved by holding group calls where we discuss the documentation together, where attendees are invtied to consider how the process would work with their research.

Notes from these consultation meetings are available below:
- [Meeting 1: Process 1-3](./docs/CallNotes-SoftLanuch-process1-3.md)
- [Meeting 2: Process 4-5](./docs/CallNotes-SoftLanuch-process4-5.md)
- [Meeting 3: General Questions](./docs/CallNotes-SoftLanuch-outstandingQuestions.md)

### Create new resources
Following the consultation, it is likely that we will need to develop a number of additional resources to support the implementation of open data sharing. We would love to involve you in the creation of these resources, if you are interested! Any new resources needed will be listed as [issues in this repository](https://git.fmrib.ox.ac.uk/open-science/community/data-sharing-decision-tree/-/issues).

New resources will be developed openly on GitLab. We will provide full training and support to anyone not familiar with working in this way.

## 4. Acknowledgements
Any and all contributors to the development of this resource will be listed in the [ACKNOWLEDGEMENTS](ACKNOWLEDGEMENTS.md) for this project. They will also be listed as authors in the zenodo entry for the project digital object identifier (see [issue #3](https://git.fmrib.ox.ac.uk/open-science/community/data-sharing-decision-tree/-/issues/3))
